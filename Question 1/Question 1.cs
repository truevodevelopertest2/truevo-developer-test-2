﻿using System;

namespace Question_1
{
    class Program
    {
        /// <summary>
        /// Sum the total of multiples below 900 of 2 or 4
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            int sum = 0;

            for (int i=0; i<900; i++)
            {
                
                if (i % 2 == 0 || i % 4 == 0)
                {

                    sum = sum += i;

                    if (i == 898)
                    {
                        Console.WriteLine("Value of i is: " + i);

                        Console.WriteLine("Sum the total of multiples below 900 of 2 or 4 is: " + sum);

                        Console.WriteLine("Press Any Key To Exit");
                        Console.ReadKey();
                    }
                    
                }

            }

        }
    }
}
