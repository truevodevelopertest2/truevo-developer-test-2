﻿using System;

namespace Question_10
{
    class Question_10
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Data Source should be used instead of server.");
            Console.WriteLine("            ");
            Console.WriteLine("No need to declare A as SQL parameter, it's been declared as string");
            Console.WriteLine("            ");
            Console.WriteLine("Use Parameters.AddWithValue");
            Console.WriteLine("            ");
            Console.WriteLine("var WebServices service = new WebServices.Service()");
            Console.WriteLine("            ");
            Console.WriteLine("DoWork should return a value of 1 or 0. ");
            Console.WriteLine("            ");
            Console.WriteLine("Add an import for WebServices");
            Console.ReadKey();
        }
    }
}
