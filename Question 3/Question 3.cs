﻿using System;

namespace Question_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;

            for (int i = 1; i < 3000000; i++)
            {

                if (i % 2 != 0 && i< 3000000)
                {
                    sum = sum += i;

                    if (i< 3000000)
                    {
                        Console.WriteLine("Value of largest odd is: " + i);

                        Console.WriteLine("Sum total of the odd (1, 3, 5, ...) numbers : " + sum);

                        Console.WriteLine("Press Any Key To Continue");

                        Console.ReadKey();
                    }
                }
                
            }
        }
    }
}
