﻿using System;

namespace Question_8
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;

            int prime = 10000;

            for (int i = 0; i < prime; i++)
            {

                if (i % 7 == 3)
                {

                    sum = sum += i;

                    if (i < prime && i != 10)
                    {
                        Console.WriteLine("Value of i is: " + i);

                        Console.WriteLine("          ");

                        Console.WriteLine("Sum the values which have a remainder of 3 when divided by 7: " + sum);

                        Console.WriteLine("          ");
                        Console.WriteLine("Press Any Key To Exit");
                        Console.ReadKey();
                    }

                }

            }
        }
    }
}
