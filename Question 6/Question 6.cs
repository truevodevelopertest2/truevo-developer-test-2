﻿using System;

namespace Question_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Fibonacci Sequence details that each number is determined by the sum of two previous numbers.");

            Console.WriteLine("Rule used to determine the  term is xn = xn-1 + xn-2. To get 2nd term I replaced n with 2 and the value for term is 1");

            Console.ReadLine();
        }
    }
}
