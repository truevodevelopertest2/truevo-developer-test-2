﻿using System;

namespace Question_5
{
    class Program
    {
        static void Main(string[] args)
        {
            int smallInt = 20;

            for (int i = 2; i <= smallInt; i++)
            {

                if (smallInt % i != 0)
                {
                    Console.WriteLine(i);
                    Console.ReadLine();

                }
                else
                {
                    continue;
                }
            }
        }

    }

}


