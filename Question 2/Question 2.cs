﻿using System;

namespace Question_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Write the solution to use JSON based Web Service");

            Console.WriteLine("Write the solution to use Asynchronous/Await request handling");

            Console.WriteLine("Ensure that each class/object has a single responsibility");

            Console.WriteLine("Update error messages to be clear and straight forward");
        }
    }
}
