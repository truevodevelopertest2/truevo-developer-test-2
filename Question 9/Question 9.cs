﻿using System;

namespace Question_9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Reference Types don't accept null values.");

            Console.WriteLine("          ");

            Console.WriteLine("Allow Nullable Reference Types to accept nulls. i.e, ensuring that default value of 0 is not assigned to an int variable if not provided. ");
        }
    }
}
